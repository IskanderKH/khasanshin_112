import threading
import requests
import shutil

file_path = "result/user_info.txt"


def get_dog(url_):
    n = 3   # всего 12 фотографии / 4 потока
    for i in range(n):
        img_url = requests.get(url_).json()['message']
        img = requests.get(img_url, stream=True)
        img_name = img_url.split("/")[-1]
        filename = f"result/{img_name}"
        img.raw.decode_content = True
        with open(filename, 'wb') as f:
            shutil.copyfileobj(img.raw, f)


url_ip = 'https://api.ipify.org/?format=json'
ip = requests.get(url_ip)
url_info = f"https://ipinfo.io/{ip.json()['ip']}/geo"
url_dog = 'https://dog.ceo/api/breeds/image/random'

info = requests.get(url_info)

# запись 1-2 заданий
with open(file_path, mode='w') as file:
    file.write(ip.text + '\n')
    file.write(info.text)

# 4 потока
th1 = threading.Thread(target=get_dog(url_dog))
th2 = threading.Thread(target=get_dog(url_dog))
th3 = threading.Thread(target=get_dog(url_dog))
th4 = threading.Thread(target=get_dog(url_dog))

th1.start()
th2.start()
th3.start()
th4.start()