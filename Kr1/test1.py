class LegoBlock:
    def __init__(self, height, width, length, weight, color=None):
        self.height = height
        self.width = width
        self.length = length
        self.weight = weight
        self.color = color

    


class Shop:
    def __init__(self):
        self.stack = []

    def push(self, item, color):
        setattr(item, 'color', color)
        self.stack.append(item)

    def pop(self):
        if len(self.stack) == 0:
            return None
        removed = self.stack.pop()
        return removed

    def back(self):
        last = self.stack[-1]
        return last

    def size(self):
        size = len(self.stack)
        return size

    def clear(self):
        self.stack = []
        return "OK"


i1 = LegoBlock(10, 5, 4, 2)
i2 = LegoBlock(10, 5, 4, 2)
shop = Shop()
shop.push(i1, "black")


def wtite_txt(shop):
    with open("info.txt", mode="w", encoding="UTF-8") as file:
        file.write(f"В цеху {shop.size()} деталей:\n")
        for i in range(shop.size()):
            file.write(f"Деталь 1: {shop.stack[i].height}, {shop.stack[i].width}, {shop.stack[i].length},"
                       f" {shop.stack[i].weight}, {shop.stack[i].color}\n")


wtite_txt(shop)